Manual pages
============

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   dleyna-renderer-service
   dleyna-renderer-service.conf
   dleyna-server-service
   dleyna-server-service.conf
