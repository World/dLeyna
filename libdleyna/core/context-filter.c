/*
 * dLeyna
 *
 * Copyright (C) 2012-2017 Intel Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Ludovic Ferrandis <ludovic.ferrandis@intel.com>
 *
 */

#include <config.h>

#include <string.h>

#include "context-filter.h"
#include "log.h"

struct dleyna_context_filter_t_ {
	GUPnPContextFilter *gupnp_cf;
};

#if DLEYNA_LOG_LEVEL & DLEYNA_LOG_LEVEL_DEBUG
static void prv_dump_cf_entries(GUPnPContextFilter *cf)
{
	GList *l;
	GList *filters;

	filters = l = gupnp_context_filter_get_entries(cf);

	DLEYNA_LOG_DEBUG_NL();
	DLEYNA_LOG_DEBUG("Context Filter entries:");

	if (l != NULL) {
		while (l != NULL) {
			DLEYNA_LOG_DEBUG("     Entry: [%s].", (char *)l->data);
			l = l->next;
		}
	} else {
		DLEYNA_LOG_DEBUG("     Context Filter Empty.");
	}

	DLEYNA_LOG_DEBUG_NL();

	g_list_free(filters);
}
#endif

dleyna_context_filter_t *dleyna_context_filter_new(GUPnPContextFilter *gupnp_cf)
{
	dleyna_context_filter_t *cf;

	if (gupnp_cf != NULL) {
		cf =  g_new0(dleyna_context_filter_t, 1);

		cf->gupnp_cf = gupnp_cf;
	} else {
		cf = NULL;
		DLEYNA_LOG_DEBUG("Parameter must not be NULL");
	}


	return cf;
}

void dleyna_context_filter_delete(dleyna_context_filter_t *cf)
{
	g_free(cf);
}

void dleyna_context_filter_enable(dleyna_context_filter_t *cf,
			      gboolean enabled)
{
	if (cf->gupnp_cf != NULL) {
		gupnp_context_filter_set_enabled(cf->gupnp_cf, enabled);

		DLEYNA_LOG_DEBUG("White List enabled: %d", enabled);
	}
}

void dleyna_context_filter_add_entries(dleyna_context_filter_t *cf,
				   GVariant *entries)
{
	GVariantIter viter;
	gchar *entry;

	DLEYNA_LOG_DEBUG("Enter");

	if ((entries != NULL) && (cf->gupnp_cf != NULL)) {
		(void) g_variant_iter_init(&viter, entries);

		while (g_variant_iter_next(&viter, "&s", &entry))
			(void) gupnp_context_filter_add_entry(cf->gupnp_cf, entry);

#if DLEYNA_LOG_LEVEL & DLEYNA_LOG_LEVEL_DEBUG
		prv_dump_cf_entries(cf->gupnp_cf);
#endif
	}

	DLEYNA_LOG_DEBUG("Exit");
}

void dleyna_context_filter_remove_entries(dleyna_context_filter_t *cf,
				      GVariant *entries)
{
	GVariantIter viter;
	gchar *entry;

	if ((entries != NULL) && (cf->gupnp_cf != NULL)) {
		(void) g_variant_iter_init(&viter, entries);

		while (g_variant_iter_next(&viter, "&s", &entry))
			(void) gupnp_context_filter_remove_entry(cf->gupnp_cf,
							     entry);

#if DLEYNA_LOG_LEVEL & DLEYNA_LOG_LEVEL_DEBUG
		prv_dump_cf_entries(cf->gupnp_cf);
#endif
	}
}

void dleyna_context_filter_clear(dleyna_context_filter_t *cf)
{
	if (cf->gupnp_cf != NULL) {
		DLEYNA_LOG_DEBUG("Clear white list");
		gupnp_context_filter_clear(cf->gupnp_cf);

#if DLEYNA_LOG_LEVEL & DLEYNA_LOG_LEVEL_DEBUG
		prv_dump_cf_entries(cf->gupnp_cf);
#endif
	}
}
