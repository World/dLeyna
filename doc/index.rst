.. SPDX-License-Identifier: LGPL-2.1-or-later
.. dLeyna documentation master file, created by
   sphinx-quickstart on Wed Jul 27 22:56:17 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================
Welcome to dLeyna's documentation!
==================================
.. note::
   This documentation is currently a stub and work in progress. It only provides a bare minumum of information

About
=====

dLeyna is a set of services and :term:`D-Bus` APIs that aim to simplify access to :term:`UPnP` and :term:`DLNA` media devices in a network.

The latest version of this document can be found at https://world.pages.gitlab.gnome.org/dLeyna/docs/

The current development of the project is hosted at GNOME's gitlab instance: https://gitlab.gnome.org/World/dLeyna

.. toctree::
   :maxdepth: 2
   :hidden:

   man-pages
   dbus-api
   logging
   references
   glossary



Services
--------
dLeyna-server provides APIs for discovering Digital Media Servers and for browsing and searching their contents.
It can be used in conjunction with a multimedia framework, such as GStreamer, to implement a Digital Media Player. It
can also be used to implement the DLNA Download System Use Case.

dLeyna-renderer provides APIs for discovering and manipulating Digital Media Renderers. It can be used to implement
two-box push. Together, these components can be used to create a Digital Media Controller.

Acknowledgements
================

dLeyna has its origins in Intel's OSTC. The original repositories can be found at

- https://github.com/intel/dleyna-core
- https://github.com/intel/dleyna-connector-dbus
- https://github.com/intel/dleyna-server
- https://github.com/intel/dleyna-renderer

The old documentation can still be reached using archive.org's wayback machine:

https://web.archive.org/web/20210612235137/https://01.org/dleyna/

